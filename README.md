# Personal List of Songs with Chords for Guitar

This repository contains my personal list of songs with chords for guitar I like to play. I play mostly country/folk Czech or English songs, but there are plenty more. The songs are mostly country/folk in Czech and sometimes in English, too. Maybe, you will find a song or two here which you will like, too. Feel free to look around, play, redistribute and most importantly, have a good time and enjoy.

# About

I love to play my guitar, but chords for songs I like to play are not well-written, does not sound right to me or are missing entirely. Furthermore, I have repository with me everywhere I go so if the opportunity occurs to play and sing a song, I can open a simple plain text file anywhere on any device. What is even better is that I can simply send the file to other people on their mobile phones, for example, and they can then join themselves. Throughout the years, I have created my own format of writing chords for songs and formatting the whole song in a manner that suits me and my monitor/tablet.

## Licence
Personal List of Songs with Chords for Guitar

CC BY-NC-SA 4.0 Licence \
David Chocholatý (Adda) \
See [LICENSE](LICENSE)
